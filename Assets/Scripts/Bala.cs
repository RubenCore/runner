﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public float speed=15f;

    

    void Start()
    {
        // este GameObject es para cunado pase x tiempo
        GameObject.Destroy(this.gameObject,0.6f);
    }

    void Update()
    {
        //Vector2.right = new Vector2(1,0)
        //multiplicar Time.deltaTime traspasa de frame a segundo
        this.transform.Translate(Vector2.right * speed * Time.deltaTime);
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Meteoritos")
        {
            GameObject.Destroy(other.gameObject);
            // este GameObject es para cunado colisiona con el meteoritos
            GameObject.Destroy(this.gameObject);
            //busca objeto en escena con el script controller para ir a la funcion de modificar
            GameObject.FindObjectOfType<controller>().GetComponent<controller>().modificarScore();
        }
    }
}
