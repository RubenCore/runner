﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class controller : MonoBehaviour
{
    //prefab
    public GameObject playerGO;
    //player es el objeto de la escena 
    private PJ player;
    public Camera gameCamera;
    public GameObject pantallaMuerte; 
    public GameObject textoScore;
    //tooltip= sale un comentario en el apartado de unity correspondiente
    [Tooltip("Galletas = Sitio donde aparecen los cometas")]public Transform galletas;
    private int score;
    //generacion de prefabs
    //lista de prefabs
    public GameObject[] mapPrefabs;

    public float lastTime;
    public float operacion;


    public GameObject[] fondo;

    
    void Start()
    {
        lastTime = Time.time;
        pantallaMuerte.SetActive(false);
        textoScore.SetActive(true);

        //inicializa el prefab y lo guarda en player
        player = Instantiate(playerGO,this.transform).GetComponent<PJ>();
        //se inicializa el spawn
        GameObject newMap = Instantiate(mapPrefabs[0], galletas);
        newMap.transform.position = new Vector3(
            player.transform.position.x + 15,
            newMap.transform.position.y,
            newMap.transform.position.z
            );
        
        InvokeRepeating("createFondo", 1f,4f);  
    }

   
    void Update()
    {
        operacion = (3.5f * player.velocidadHorizontalInicial) / player.velocidadHorizontalSube;

        //el if hace la operacion del tiempo actual mas la cantidad de tiempo y si es mas pequeño que el tiempo actual hace un mapa nuevo,
        //siempre hay que actualizar el lastTime con el Time.time, si el Time.time lo calcula en milisegundos hay que multiplicar la operacion por 1000
        if (lastTime+operacion <= Time.time)
        {
            Invoke("randomMap",0f);
            lastTime = Time.time;
        }

        //mvimiento camara sigue pj
        gameCamera.transform.position = new Vector3(
            player.transform.position.x,
            player.transform.position.y,
            gameCamera.transform.position.z
            );

        if (player.dead)
        {
            galletas.gameObject.SetActive(false);
            pantallaMuerte.SetActive(true);
            textoScore.SetActive(false);
            //pilla el primer hijo de este objeto que en este caso es el texto
            pantallaMuerte.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText("Score: " + score);

        }

    }

    //generacion de mapa aleatoria
    private void randomMap()
    {           
        if (!player.dead)
        {
            int random = Random.Range(1, mapPrefabs.Length);
            GameObject newMap = Instantiate(mapPrefabs[random], galletas);
            newMap.transform.position = new Vector3(
                player.transform.position.x + 15,
                newMap.transform.position.y,
                newMap.transform.position.z
                );
        }        
    }
    public void modificarScore()
    {
        score++;
        textoScore.GetComponent<TMPro.TextMeshProUGUI>().SetText("Score = "+score);
    }

    public void CambiarScena(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    //crea el "fondo parrallax" realmente aplico el parallax en los planetas, tengo una array de planetas y voy cogiendo aleatoriamente 1 y lo creo a 15 unidades de la x del personaje
    void createFondo()
    {
        int randomFondo = Random.Range(0, fondo.Length);
        GameObject GBFondo = (GameObject)Instantiate(fondo[randomFondo], player.transform);
        GBFondo.transform.position = new Vector2(player.transform.position.x + 15, transform.position.y);
        GBFondo.transform.parent = transform.parent;
        GBFondo.transform.localScale = new Vector2(.8f, .8f); 
        Destroy(GBFondo, 10f);
    }



}
