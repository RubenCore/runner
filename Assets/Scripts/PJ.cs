﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PJ : MonoBehaviour
{
    public bool dead = false;
    public float velocidadHorizontalInicial = 5f;
    public float velocidadHorizontalSube;
    public GameObject bala;

    private void Start()
    {
        Invoke("igualarVel", 0f);
        InvokeRepeating("subirVelocidad", 6f, 7f);
    }

    void Update()
    {
        //aplico una velocidad para que se mueva el peronaje
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidadHorizontalSube, this.GetComponent<Rigidbody2D>().velocity.y);

        //movimiento del personaje a partir de furzas
        if (Input.GetKey(KeyCode.W))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 15));
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -15));
        }

      
        if (Input.GetKeyDown(KeyCode.Space))
        {
            disparar();
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //colision con meteoritos
        if (collision.gameObject.tag == "Meteoritos")
        {
            muerte();
        }
    }

    //si colisiona con un meteorito printea en consola un mensaje, canvia el bool de dead a true y esconde el objeto
    private void muerte()
    {
        print("muerto");
        this.dead = true;
        this.gameObject.SetActive(false);
    }
    //accion de deispara
    private void disparar()
    {
        if (!dead)
        {
            //guarda la bala para poder modificar la posicion
            GameObject aux = (GameObject)Instantiate(bala);
            //modifica la posicion
            aux.transform.position = this.transform.position;
        }

    }
    //iguala la variable de velocidad inicial al la variable de velocidad que sube
    private void igualarVel()
    {
        velocidadHorizontalSube = velocidadHorizontalInicial;
    }
    //sube la velocidad 
    private void subirVelocidad()
    {       

        if (velocidadHorizontalSube < 7f)
        {
            velocidadHorizontalSube++;
        }
       
    }

}
